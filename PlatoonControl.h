//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef PLATOONCONTROL_H_
#define PLATOONCONTROL_H_

#include "veins/modules/application/ieee80211p/DemoBaseApplLayer.h"
#include "PlatoonMsg_m.h"
#include <map> // Header file to include map in the code
#include <string>
#include<unordered_map>
#include<iostream>

namespace veins{
class VEINS_API PlatoonControl: public DemoBaseApplLayer {
public:
    void initialize(int stage) override;

protected:
    int currentSubscribedServiceId;
    int currentSubscribedChannel;

    cMessage* wsmSendEvt;   // event for wsm transmission
    cMessage* ctrlEvt; // event for calling the control law for this vehicle
    cMessage* accUpdateEvt; // event for updating the acceleration value of this vehicle

    simtime_t sendPeriod;
    simtime_t controlPeriod;

    Coord myAcc;   // my up-to-date acceleration
    double past_error;
    Coord lastSpeed;   // speed from the last time slot (presumably "controlPeriod" time ago)

    bool firstMsgRcved;// false when this vehicle has not yet received any wsm msg
    bool rsu_prox;
    PlatoonMsg lastRcvMsg;// lastly received message (from the vehicle in front)
    double lastCloserDist;
    int vehicleInfrontId;

    double distLeader;

    std::unordered_map<std::string, int> mp; //creating a map

    void onWSM(BaseFrame1609_4* wsm) override;
    void onWSA(DemoServiceAdvertisment* wsa) override;

    void handleSelfMsg(cMessage* msg) override;

    void sendWsm();
    void controlLaw();
    void setAccel(double acc);
};
}

#endif /* PLATOONCONTROL_H_ */
