//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "MyRSU.h"

#include "PlatoonMsg_m.h"
using namespace veins;

#define WSM_EVT 70
//#define BSM_EVT 80
//using std::cout;
//using std::cin;

Define_Module(veins::MyRSU);

void MyRSU::initialize(int stage) {
    DemoBaseApplLayer::initialize(stage);
    if (stage == 0) {
        currentOfferedServiceId = 17; // whatever number you'd like
        sendPeriod = 0.03;

        wsmSendEvt = new cMessage("wsm send task", WSM_EVT);
        //bsmSendEvt = new cMessage("bsm send task", BSM_EVT);

    } else if (stage == 1) {
        startService(Channel::sch1, currentOfferedServiceId, "RSU Broadcast Service");
        simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);
        scheduleAt(wsm_time, wsmSendEvt); // and send wsm for the second vehicle

        //simtime_t bsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);
        //scheduleAt(wsm_time, bsmSendEvt); // and send wsm for the second vehicle

    }
}

void MyRSU::onWSM(BaseFrame1609_4 *frame) {

    if (PlatoonMsg* pMsg = check_and_cast<PlatoonMsg*>(frame)){
        /*Coord senderPos = pMsg->getSenderPos();
        double distance = (senderPos - curPosition).length();
        std::cout << "CarId: " << pMsg->getSenderAddress() << " Distance: " << distance << "\n";
        if(distance < 10){
            std::cout << "Car " << pMsg->getSenderAddress() << " arrived at the Y intersection! " << distance << "\n";
        }*/
        //std::cout << "RSU ID: " << myId << "Received message from V.id: " << pMsg->getSenderAddress() << std::endl;
        }
}


void MyRSU::onWSA(DemoServiceAdvertisment* wsa){


}




void MyRSU::handleSelfMsg(cMessage* msg) {
    switch (msg->getKind() ) {
    case WSM_EVT:{   // this event prepares a message and send to others
        sendWsm();
        simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);
        scheduleAt(wsm_time, wsmSendEvt);
        break;
    }/*
    case BSM_EVT:{
        sendBsm();
        simtime_t bsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);
        scheduleAt(bsm_time, bsmSendEvt);
        break;
    }*/
    default:{
        DemoBaseApplLayer::handleSelfMsg(msg);
        break;
    }
    }
}

void MyRSU::sendWsm() {
    PlatoonMsg* RSUwsm = new PlatoonMsg();
    RSUwsm->setSenderAddress(myId);
    //RSUwsm->Coord senderAcc;
    RSUwsm->setTimeStampP(simTime());
    //RSUwsm->setChannelNumber(static_cast<int>(currentSubscribedChannel));
    RSUwsm->setType(1);
    RSUwsm->setSenderPos(curPosition);
    sendDown(RSUwsm->dup());
    delete RSUwsm;
    std::cout << "WSM from RSU sent at " << simTime() << "At channel: " << currentSubscribedChannel << std::endl;
    //scheduleAt(simTime() + sendPeriod, wsmSendEvt);
}

/*void MyRSU::sendBsm() {
    PlatoonMsg* RSUbsm = new PlatoonMsg();
    RSUbsm->setSenderAddress(myId);
    //RSUwsm->Coord senderAcc;
    RSUbsm->setTimeStampP(simTime());
    //RSUwsm->setChannelNumber(static_cast<int>(currentSubscribedChannel));
    RSUbsm->setType(1);
    RSUbsm->setSenderPos(curPosition);
    sendDown(RSUbsm->dup());
    delete RSUbsm;
    std::cout << "BSM from RSU sent at " << simTime() << "At channel: " << currentSubscribedChannel << std::endl;
    //scheduleAt(simTime() + sendPeriod, wsmSendEvt);
}*/



