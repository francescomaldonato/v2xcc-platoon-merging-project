# V2XCC platoon merging project
Open V2XCC_report_Jagdish_Maldonato.pdf for complete info on the project.
Open YintersectionFlow_V2XCC_project_presentation_final.pdf for visual presentation and demo.

## Installation
Getting started with Omnet++: https://docs.omnetpp.org/tutorials/tictoc/part1/
Veins installation: https://veins.car2x.org/tutorial/

## Run
-Open this repository with Omnet++
-Run the omnetpp.ini file

## Roadmap
Listed in the project report.

## Authors and acknowledgment
Francesco Maldonato: f.maldonato@campus.tu-berlin.de
Nandu Jagdish: nandu.jagdish@campus.tu-berlin.de
