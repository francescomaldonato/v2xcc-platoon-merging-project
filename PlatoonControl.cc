//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 



#include "PlatoonControl.h"

#define RSU_EVT 12

#define WSM_EVT 15
#define CTRL_EVT 35
#define ACC_UPDATE_EVT 55

using namespace veins;

Define_Module(veins::PlatoonControl);

void PlatoonControl::initialize(int stage) {
    DemoBaseApplLayer::initialize(stage);
    if (stage ==0 ) {

        currentSubscribedServiceId = -1;
        sendPeriod = 0.05;
        controlPeriod = 0.05;

        past_error = 0.0;
        lastSpeed = curSpeed;

        firstMsgRcved = false;
        rsu_prox = false;

        wsmSendEvt = new cMessage("wsm send task", WSM_EVT);
        ctrlEvt = new cMessage("control task", CTRL_EVT);
        accUpdateEvt = new cMessage("acceleration update task", ACC_UPDATE_EVT);

        mp.insert(std::pair<std::string, int>("flow0.0", 16));  //inserting values
        mp.insert(std::pair<std::string, int>("flow1.0", 22));
        mp.insert(std::pair<std::string, int>("flow0.1", 28));
        mp.insert(std::pair<std::string, int>("flow1.1", 34));
        mp.insert(std::pair<std::string, int>("flow0.2", 40));
        mp.insert(std::pair<std::string, int>("flow1.2", 46));  //inserting values
        mp.insert(std::pair<std::string, int>("flow0.3", 52));
        mp.insert(std::pair<std::string, int>("flow1.3", 58));
        mp.insert(std::pair<std::string, int>("flow0.4", 64));
        mp.insert(std::pair<std::string, int>("flow1.4", 70));

    }else if (stage == 1) {

        if (myId == 16 || myId == 22) { // if it's the first vehicle of the 2 platoons
            currentOfferedServiceId = 1;
            startService(Channel::sch2, currentOfferedServiceId, "Platoon Service");
            simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);

            scheduleAt(wsm_time, wsmSendEvt); // and send wsm for the second vehicle
            scheduleAt(simTime(), ctrlEvt);
        }
        if (myId != 10 && myId != 16) {
            vehicleInfrontId = (myId - 12);
            // due to some reason, the vehicle in front has such an ID (-12)
        }
        scheduleAt(simTime(), accUpdateEvt);
    }
}


#define MAX_SENSOR_RANGE_WSM 200.0
#define RSU_RANGE 150.0

void PlatoonControl::onWSM(BaseFrame1609_4* frame){
    //std::cout << "Vehicle_id: " << myId << "Received by id: " << pMsg->getSenderAddress() <<  ", subs id: " << currentSubscribedServiceId << ", ch: " << currentSubscribedChannel << std::endl;
    if (PlatoonMsg* pMsg = check_and_cast<PlatoonMsg*>(frame)){
        if(pMsg->getType() == 1){
            //std::cout << "Vehicle_id: " << myId << "Received WSM by RSU" << std::endl;
            Coord senderPos = pMsg->getSenderPos();
            double distanceRSU = (senderPos - curPosition).length();
            std::cout << "WSM RECEIVED FROM RSU by CarId: " << myId << " Distance: " << distanceRSU << "\n";
                    if(distanceRSU < RSU_RANGE){
                        std::cout << "Car Id #" << myId << " close to Y intersection! " << distanceRSU << "\n";
                        rsu_prox = true;
                        simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);

                    }else{
                        rsu_prox = false;
                    }
        }else{
            Coord senderPos = pMsg->getSenderPos();
            double distanceVehicle = (senderPos - curPosition).length();
            std::cout << "Vehicle " << myId << " received WSM by: " << pMsg->getSenderAddress() << " with distance " << distanceVehicle << std::endl;

            std::pair<std::string, double> leaderInfo = traciVehicle->getLeader(MAX_SENSOR_RANGE_WSM);

            if ( leaderInfo.first.empty() ){
                EV_ERROR << "No one in front of Veh. Id: " << myId << std::endl;
            }else{

            vehicleInfrontId = mp.find(leaderInfo.first)->second;
            std::cout << "Vehicle Id: " << myId <<  " has in front Vehicle: " << vehicleInfrontId << " with code " << leaderInfo.first << std::endl;
            distLeader = leaderInfo.second;
            }

            if ( pMsg->getSenderAddress() == vehicleInfrontId ) { // if the data is from the vehicle in front
                // note that the lead vehicle will not enter this IF clause
                lastCloserDist = distanceVehicle;
                std::cout << "VEHICLE " << myId << " IN FRONT see Vehicle: " << pMsg->getSenderAddress() << " with distance " << distanceVehicle << ", DistLeader: " << distLeader << std::endl;
                std::cout << "Id: " << myId << "position: " << curPosition << ", vehicle " << pMsg->getSenderAddress() << " position: " << senderPos << std::endl;
                lastRcvMsg = *pMsg;
                // copy pMsg into lastRcvMsg. The data will be used in the controller algorithm
                if ( !firstMsgRcved ) // if this is the first received wsm message
                    firstMsgRcved = true;
            }
        }
    }
}

void PlatoonControl::onWSA(DemoServiceAdvertisment* wsa){
    std::cout << "WSA received from Vehicle " << myId << ", psid: " << currentSubscribedServiceId << ", subscribed to channel " << currentSubscribedChannel << std::endl;

    currentSubscribedServiceId = wsa->getPsid();
    currentSubscribedChannel = wsa->getTargetChannel();
    mac->changeServiceChannel((Channel)(currentSubscribedChannel));
    simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);

    if(!(wsmSendEvt->isScheduled())){
        scheduleAt(wsm_time, wsmSendEvt);
    }
    if(!(ctrlEvt->isScheduled())){
        scheduleAt(simTime(), ctrlEvt);
    }
}

void PlatoonControl::handleSelfMsg(cMessage* msg) {
    switch (msg->getKind() ) {

    case WSM_EVT:{   // this event prepares a message and send to others
        sendWsm();
        simtime_t wsm_time = computeAsynchronousSendingTime(sendPeriod, ChannelType::service);
        if(wsmSendEvt->isScheduled()){
            break;
        }else{
            scheduleAt(wsm_time, wsmSendEvt);
            break;
        }
    }
    case CTRL_EVT:{
        controlLaw();
        if(ctrlEvt->isScheduled()){
                break;
            }else{
                scheduleAt(simTime()+controlPeriod, ctrlEvt);
                break;
            }
        }
    case ACC_UPDATE_EVT:{
        myAcc = (curSpeed - lastSpeed)*(1/controlPeriod);   // curSpeed is automatically updated from somewhere in the inheritance tree
        lastSpeed = curSpeed;   // myAcc is not, so we manually do it here. Otherwise, we won't have an acceleration value stored in myAcc
        if(accUpdateEvt->isScheduled()){
                   break;
               }else{
                   scheduleAt(simTime()+controlPeriod, accUpdateEvt);
                   break;
               }
        }

    default:{
        DemoBaseApplLayer::handleSelfMsg(msg);
        break;
    }
    }
}

void PlatoonControl::sendWsm() {
    PlatoonMsg* pMsg = new PlatoonMsg();
    pMsg->setType(0);
    pMsg->setSenderPos(curPosition);
    pMsg->setSenderAddress(myId);
    pMsg->setSenderAcc(myAcc);
    pMsg->setTimeStampP(simTime());
    pMsg->setChannelNumber(static_cast<int>(currentSubscribedChannel));
    sendDown(pMsg->dup());
    delete pMsg;
    //std::cout << "WSM sent at " << simTime() << std::endl;
    //scheduleAt(simTime() + sendPeriod, wsmSendEvt);
}

// let's define controller parameters here
#define DESIRED_DIST 5
#define RSU_DIST 10

#define K1 0.5
#define K2 3.0
#define K3 2.0
#define MAX_SENSOR_RANGE 200.0

void PlatoonControl::controlLaw() {
        // read sensors
        std::pair<std::string, double> leaderInfo = traciVehicle->getLeader(MAX_SENSOR_RANGE);
        // getLeader gets you the distance to the vehicle in front
        // read https://sumo.dlr.de/docs/TraCI/Vehicle_Value_Retrieval.html and look for getLeader() there
        if ( leaderInfo.first.empty() ){
            EV_ERROR << "I can't sense anything in front. Veh. Id: " << myId << std::endl;}
        double dist = leaderInfo.second;

        if ( leaderInfo.second == -1 ){
            //std::cout << "MyId: " << myId << ", NO vehicles in front of me. " << std::endl;
            double free_acc = 3.0;
            this->setAccel(free_acc);
        }else{
        std::string leaderId = leaderInfo.first;

        double desired_dist = DESIRED_DIST;
        if (rsu_prox) {
            desired_dist = RSU_DIST;
        }

        double range_error = dist - desired_dist;
        double range_rate_error = (range_error - past_error)/controlPeriod;
        double acc_error = lastRcvMsg.getSenderAcc().length()-myAcc.length();  // getSenderAcc returns a Coord vector, so I call length()
        double acc = K1* range_error + K2*range_rate_error + K3* acc_error;

        // display to console for debugging purposes
        //int vehicle_no = 4; //lead vehicle: 1, second vehicle: 2
        if ( myId == 58 || myId == 64) {
            std::cout << "dist: " << dist <<", err: " << range_error << ", err_rate: " << range_rate_error << ", acc_err: " << acc_error << ", acc: " << acc << std::endl;
        }
        this->setAccel(acc);
        past_error = range_error;
        //double speed = traciVehicle->getSpeed(); // if you want to use the speed of this vehicle, uncomment this line

    }
}

void PlatoonControl::setAccel(double acc) {
    if ( acc > 0.0 ) {
        //if ( acc > 4.0 ) acc = 4.0;
        traciVehicle->setAccel(acc);
        traciVehicle->setSpeedMode(0x06);
        traciVehicle->setSpeed(100.0);
    } else {
        if ( acc < -6.0) acc = -5.0;
        traciVehicle->setDecel(-acc);
        traciVehicle->setSpeedMode(0x06);
        traciVehicle->setSpeed(0.0);
    }
}

