//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#pragma once
#include "veins/modules/application/ieee80211p/DemoBaseApplLayer.h"
namespace veins {
/**
* Small RSU Demo using 11p
*/
class VEINS_API MyRSU : public DemoBaseApplLayer {
public:
void initialize(int stage) override;


protected:


int currentSubscribedServiceId;
int currentSubscribedChannel;
cMessage* wsmSendEvt;   // event for wsm transmission
//cMessage* bsmSendEvt;   // event for bsm transmission

simtime_t sendPeriod;


void onWSM(BaseFrame1609_4* wsm) override;
void onWSA(DemoServiceAdvertisment* wsa) override;

void handleSelfMsg(cMessage* msg) override;

void sendWsm();
//void sendBsm();


}; // namespace veins
}

